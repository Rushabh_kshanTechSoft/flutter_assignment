import 'package:flutter/material.dart';
import 'package:flutter_assignment/progressComponent.dart';
import 'package:flutter_assignment/signUpThirdPage.dart';

class signUpSecondPage extends StatefulWidget {
  @override
  _signUpSecondPageState createState() => _signUpSecondPageState();
}

class _signUpSecondPageState extends State<signUpSecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          elevation: 0,
          title: Text('Create Account'),
          centerTitle: false,
          backgroundColor: Colors.blue,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //progressBar(), display progress componants
Padding(
  padding: const EdgeInsets.fromLTRB(12.0,0.0,12.0,0.0),
  child:   Row(
                children: <Widget>[
                  circle_element(
                    bgcolor: Colors.green,
                    number: '1',
                  ),
                  line_element(),
                  circle_element(
                    bgcolor: Colors.white,
                    number: '2',
                  ),
                  line_element(),
                  circle_element(
                    bgcolor: Colors.white,
                    number: '3',
                  ),
                  line_element(),
                  circle_element(
                    bgcolor: Colors.white,
                    number: '4',
                  ),
                ],
              ),
),
                Padding(
                  padding: const EdgeInsets.fromLTRB(
                    30,
                    30,
                    0,
                    5,
                  ),
                  child: Text(
                    'Create Password',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(
                    30,
                    5,
                    0,
                    5,
                  ),
                  child: Text('Password will be used to login to connect',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 14)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(
                    27,
                    40,
                    30,
                    5,
                  ),
                  child: TextFormField(
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(16.0),
                        ),
                      ),
                      // enabledBorder: UnderlineInputBorder(
                      //   borderSide: BorderSide(color: Colors.transparent),
                      // ),
                      // focusedBorder: UnderlineInputBorder(
                      //   borderSide: BorderSide(color: Colors.transparent),
                      // ),
                      filled: true,
                      fillColor: Colors.grey[200],
                      suffixIcon: Icon(
                        Icons.visibility,
                        color: Colors.blue,
                      ),
                      hintText: 'Create Password',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(
                    30,
                    20,
                    30,
                    5,
                  ),
                  child: Text('Complexity',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 14)),
                ),
                Container(
                  height: 80,
                  //color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: 80,
                          width: 80,
                          child: Column(
                            children: <Widget>[
                              Text('a',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25)),
                              Text('Lowercase',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 14)),
                            ],
                          ),
                        ),
                        Container(
                          height: 80,
                          width: 80,
                          child: Column(
                            children: <Widget>[
                              Text('A',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25)),
                              Text('Uppercase',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 14)),
                            ],
                          ),
                        ),
                        Container(
                          height: 80,
                          width: 80,
                          child: Column(
                            children: <Widget>[
                              Text('123',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25)),
                              Text('Number',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 14)),
                            ],
                          ),
                        ),
                        Container(
                          height: 80,
                          width: 80,
                          child: Column(
                            children: <Widget>[
                              Text('9+',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25)),
                              Text('Charecters',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 14)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonTheme(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => signUpThirdPage()),
                        );
                      },
                      child: Text(
                        'Next',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue[400],
                    ),
                    minWidth: 300.0,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
