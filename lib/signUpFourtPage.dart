import 'package:flutter/material.dart';
import 'package:flutter_assignment/progressComponent.dart';

class signUpFourtPage extends StatefulWidget {
  @override
  _signUpFourtPageState createState() => _signUpFourtPageState();
}

class _signUpFourtPageState extends State<signUpFourtPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(title: Text('Create Account'), centerTitle: false),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //   progressBar(),
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0.0),
                child: Row(
                  children: <Widget>[
                    circle_element(
                      bgcolor: Colors.green,
                      number: '1',
                    ),
                    line_element(),
                    circle_element(
                      bgcolor: Colors.green,
                      number: '2',
                    ),
                    line_element(),
                    circle_element(
                      bgcolor: Colors.green,
                      number: '3',
                    ),
                    line_element(),
                    circle_element(
                      bgcolor: Colors.white,
                      number: '4',
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  30,
                  30,
                  0,
                  5,
                ),
                child: Text(
                  'Schedule Video Call',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  30,
                  5,
                  25,
                  35,
                ),
                child: Text(
                    'Choose the date and time you preferred, we will send a link via email to make a video call on the scheduled date and time',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 14)),
              ),
              // ================================================ date dropdown ===============
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  25,
                  0,
                  25,
                  0,
                ),
                child: Container(
                  // margin: EdgeInsets.all(20),
                  child: TextFormField(
                    onTap: () {},
                    showCursor: false,
                    readOnly: true,
                    initialValue: '- Choose Date -',
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      labelText: 'Date',
                      labelStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.grey[200],
                      suffixIcon: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              // ==================================== End ===========================================

              // ================================================ Choose Time dropdown ===============
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  25,
                  10,
                  25,
                  25,
                ),
                child: Container(
                  // margin: EdgeInsets.all(20),
                  child: TextFormField(
                    onTap: () {},
                    showCursor: false,
                    readOnly: true,
                    initialValue: '- Choose Time -',
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      labelText: 'Time',
                      labelStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.grey[200],
                      suffixIcon: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              // ==================================== End ===========================================

              // ==================================== End ===========================================
            // Push to next screen , and change color if form is filled correctly otherwise show default color
              Positioned(
                bottom: 0,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonTheme(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      onPressed: () {
                        
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //       builder: (context) => signUpThirdPage()),
                        // );
                      },
                      child: Text(
                        'Next',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue[400],
                    ),
                    minWidth: 300.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
