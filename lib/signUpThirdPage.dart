import 'package:flutter/material.dart';
import 'package:flutter_assignment/progressComponent.dart';
import 'package:flutter_assignment/signUpFourtPage.dart';

class signUpThirdPage extends StatefulWidget {
  @override
  _signUpThirdPageState createState() => _signUpThirdPageState();
}

class goal {
  const goal(this.name);
  final String name;
}

class income {
  const income(this.name);
  final String name;
}

class expense {
  const expense(this.name);
  final String name;
}

class _signUpThirdPageState extends State<signUpThirdPage> {
  goal selectedGoal;

  List<goal> users = <goal>[
    const goal('Heigher Eduction'),
    const goal('Child future')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(title: Text('Create Account'), centerTitle: false),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
             // progressBar(),
             Padding(
  padding: const EdgeInsets.fromLTRB(12.0,0.0,12.0,0.0),
  child:   Row(
                children: <Widget>[
                  circle_element(
                    bgcolor: Colors.green,
                    number: '1',
                  ),
                  line_element(),
                  circle_element(
                    bgcolor: Colors.green,
                    number: '2',
                  ),
                  line_element(),
                  circle_element(
                    bgcolor: Colors.white,
                    number: '3',
                  ),
                  line_element(),
                  circle_element(
                    bgcolor: Colors.white,
                    number: '4',
                  ),
                ],
              ),
),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  30,
                  30,
                  0,
                  5,
                ),
                child: Text(
                  'Personal Information',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  30,
                  5,
                  25,
                  35,
                ),
                child: Text(
                    'Please fill in the information below and your goal for degital saving',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 14)),
              ),
              // ================================================ goal for activation dropdown ===============
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  25,
                  0,
                  25,
                  0,
                ),
                child: Container(
                  // margin: EdgeInsets.all(20),
                  child: TextFormField(
                    onTap: () {},
                    showCursor: false,
                    readOnly: true,
                    initialValue: '- Choose Option -',
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      labelText: 'Goal for activation',
                      labelStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.grey[200],
                      suffixIcon: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              // ==================================== End ===========================================

              // ================================================ Monthly income dropdown ===============
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  25,
                  10,
                  25,
                  0,
                ),
                child: Container(
                  // margin: EdgeInsets.all(20),
                  child: TextFormField(
                    onTap: () {},
                    showCursor: false,
                    readOnly: true,
                    initialValue: '- Choose Option -',
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      labelText: 'Monthly income',
                      labelStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.grey[200],
                      suffixIcon: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              // ==================================== End ===========================================

              // ================================================ Monthly expense dropdown ===============
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  25,
                  10,
                  25,
                  25,
                ),
                child: Container(
                  // margin: EdgeInsets.all(20),
                  child: TextFormField(
                    onTap: () {},
                    showCursor: false,
                    readOnly: true,
                    initialValue: '- Choose Option -',
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      labelText: 'Monthly expense',
                      labelStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.grey[200],
                      suffixIcon: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              // ==================================== End ===========================================
              Positioned(
                bottom: 0,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonTheme(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => signUpFourtPage()),
                        );
                      },
                      child: Text(
                        'Next',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue[400],
                    ),
                    minWidth: 300.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
