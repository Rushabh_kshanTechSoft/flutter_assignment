import 'package:flutter/material.dart';
import 'package:flutter_assignment/signUp.dart';

class circle_element extends StatefulWidget {
  @override
  String number;
  Color bgcolor;

  circle_element({this.number, this.bgcolor});

  _circle_elementState createState() => _circle_elementState();
}

class _circle_elementState extends State<circle_element> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: widget.bgcolor,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black)),
      child: Padding(
        padding: const EdgeInsets.all(13.0),
        child: Text(widget.number, style: TextStyle(fontSize: 24.0)),
      ),
    );
  }
}

class line_element extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Expanded(
      child: new Container(
          child: Divider(
        color: Colors.black,
        height: 5.0,
        thickness: 2,
      )),
    ));
  }
}

class progressBar extends StatefulWidget {
  @override
  _progressBarState createState() => _progressBarState();
}

class _progressBarState extends State<progressBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 0.0, 20.0, 0.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                circle_element(
                  bgcolor: Colors.white,
                  number: '1',
                ),
                line_element(),
                circle_element(
                  bgcolor: Colors.white,
                  number: '2',
                ),
                line_element(),
                circle_element(
                  bgcolor: Colors.white,
                  number: '3',
                ),
                line_element(),
                circle_element(
                  bgcolor: Colors.white,
                  number: '4',
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
