import 'package:flutter/material.dart';
import 'package:flutter_assignment/progressComponent.dart';
import 'dart:ui' as ui;

import 'package:flutter_assignment/signUpSecondPage.dart';

class signUp extends StatefulWidget {
  @override
  _signUpState createState() => _signUpState();
}

class _signUpState extends State<signUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: Column(
              children: <Widget>[
                // ==================================================================== Load paint and progress widget ========================
                Container(
                  color: Colors.blue,
                  height: 200,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: CustomPaint(
                      size: Size(380, 400),
                      painter: MyPainter(), //Paint widget here
                      child: Padding(
                        padding: const EdgeInsets.only(top: 40.0),
                        child: progressBar(),
                      ), //Progess widget here
                    ),
                  ),
                ),
                // ==================================================================== End  ==================================================

                // ============================================= RichText Start ==========================================
                Container(
                    color: Colors.grey[200],
                    height: MediaQuery.of(context).size.height / 2.3,
                    width: MediaQuery.of(context).size.height / 1,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 200,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: RichText(
                                text: new TextSpan(
                                  // Note: Styles for TextSpans must be explicitly defined.
                                  // Child text spans will inherit styles from parent
                                  style: new TextStyle(
                                      fontSize: 30.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                  children: <TextSpan>[
                                    new TextSpan(text: 'Welcome to GIN'),
                                    new TextSpan(
                                        text: ' Finance',
                                        style: new TextStyle(
                                            color: Colors.blueAccent)),
                                  ],
                                  // ============================================= RichText End ==========================================
                                ),
                              ),
                            ),
                          ),

                          // ============================================= Information text start ==========================================
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(20.0, 20.0, 5.0, 0.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                height: 100,
                                width: 200,
                                child: Text(
                                  'Welcome to the bank of future. manage and track your account on the go.',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),

                              // ================================================== Informaton Text End ======================
                            ),
                          ),

                          //============================================= Email box start ===================================

                          Stack(
                            //alignment:new Alignment(x, y)
                            children: <Widget>[
                              Center(
                                child: Container(
                                  height: 65,
                                  width: 300,
                                  color: Colors.white,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Container(
                                      color: Colors.white,
                                      //height: 85,
                                      child: TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent),
                                          ),
                                          filled: true,
                                          fillColor: Colors.grey[200],
                                          prefixIcon: Icon(
                                            Icons.mail,
                                            color: Colors.grey,
                                          ),
                                          hintText: 'Email',
                                          //border: OutlineInputBorder(
                                          //borderSide:
                                          //  BorderSide(color: Colors.red[300], width: 32.0),
                                          // borderRadius: BorderRadius.circular(5.0)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          //============================================= Email box end =====================================
                        ],
                      ),
                    )),
                Container(height: 100, color: Colors.grey[100]),

                // ============================================Next button =============================
                Align(
                  child: ButtonTheme(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => signUpSecondPage()),
                        );
                      },
                      child: Text(
                        'Next',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue,
                    ),
                    minWidth: 300.0,
                  ),
                  alignment: Alignment.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();

    paint.color = Colors.grey[200];

    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 3;

    var startPoint = Offset(-29, size.height / 0.7);
    var controlPoint1 = Offset(size.height / 5, size.height / 6);
    var controlPoint2 = Offset(1 * size.width / 20, size.height / 3);
    var endPoint = Offset(size.width, size.height / 1);

    var path = Path();
    path.moveTo(startPoint.dx, startPoint.dy);
    path.cubicTo(controlPoint1.dx, controlPoint1.dy, controlPoint2.dx,
        controlPoint2.dy, endPoint.dx, endPoint.dy);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}
